import { NativeModules, NativeEventEmitter } from 'react-native';
import type { TestNotificationPayload } from './types';

interface RustorePushModule {
  createPushEmitter: () => void;
  deletePushEmitter: () => void;

  getToken: () => Promise<string>;
  deleteToken: () => Promise<boolean>;

  checkPushAvailability: () => Promise<boolean>;

  subscribeToTopic: (topic: string) => Promise<boolean>;
  unsubscribeFromTopic: (topic: string) => Promise<boolean>;

  sendTestNotification: (value: TestNotificationPayload) => Promise<boolean>;

  offNativeErrorHandling: () => void;
}

export default NativeModules.RustorePush as RustorePushModule;

const eventEmitter = new NativeEventEmitter(NativeModules.RustorePush);

export { eventEmitter };
export * from './types';