package com.rustorepush.utils

enum class PushEvents {
  ON_NEW_TOKEN,
  ON_MESSAGE_RECEIVED,
  ON_DELETED_MESSAGES,
  ON_ERROR
}
