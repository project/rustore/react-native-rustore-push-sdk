package com.rustorepush.utils

import com.facebook.react.bridge.ReadableMap
import com.facebook.react.bridge.WritableArray
import com.facebook.react.bridge.WritableMap
import com.facebook.react.bridge.WritableNativeArray
import com.facebook.react.bridge.WritableNativeMap

object DataMapper {

  fun byteArrayToWritableArray(array: ByteArray?): WritableArray {
    val writableArray: WritableArray = WritableNativeArray()
    if (array != null) {
      for (i in array.indices) {
        writableArray.pushInt(array[i].toInt())
      }
    }
    return writableArray
  }

  fun mapToWritableMap(map: Map<String, String>): WritableMap {
    val result = WritableNativeMap()
    map.forEach { (key, value) ->
      result.putString(key, value)
    }
    return result
  }

   fun deconstructReadableMap(readableMap: ReadableMap?): Map<String, String> {
    val iterator = readableMap?.keySetIterator()
    val deconstructedMap: MutableMap<String, String> = mutableMapOf()
     if (iterator != null) {
       while (iterator.hasNextKey()) {
         val key = iterator.nextKey()
         deconstructedMap[key] = readableMap.getString(key)!!
       }
     }
    return deconstructedMap
  }
}
