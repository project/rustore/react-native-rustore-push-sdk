package com.rustorepush

import com.facebook.react.bridge.Promise
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod
import com.facebook.react.bridge.ReadableMap
import com.facebook.react.bridge.WritableMap
import com.facebook.react.bridge.WritableNativeMap
import com.facebook.react.modules.core.DeviceEventManagerModule
import com.rustorepush.utils.DataMapper.byteArrayToWritableArray
import com.rustorepush.utils.DataMapper.deconstructReadableMap
import com.rustorepush.utils.DataMapper.mapToWritableMap
import com.rustorepush.utils.PushEvents
import com.rustorepush.utils.RuStorePushEmitter
import ru.rustore.sdk.core.exception.RuStoreException
import ru.rustore.sdk.core.feature.model.FeatureAvailabilityResult
import ru.rustore.sdk.pushclient.RuStorePushClient
import ru.rustore.sdk.pushclient.messaging.model.RemoteMessage
import ru.rustore.sdk.pushclient.messaging.model.TestNotificationPayload
import ru.rustore.sdk.pushclient.utils.resolveForPush

class RustorePushModule(reactContext: ReactApplicationContext) :
  ReactContextBaseJavaModule(reactContext) {

  companion object {
    private var allowNativeErrorHandling = true
  }

  override fun getName(): String {
    return "RustorePush"
  }

  @ReactMethod
  fun checkPushAvailability(promise: Promise) {
    RuStorePushClient.checkPushAvailability()
      .addOnSuccessListener { result ->
        when (result) {
          is FeatureAvailabilityResult.Available -> {
            promise.resolve(true)
          }

          is FeatureAvailabilityResult.Unavailable -> {
            handleNativeError(result.cause)
            promise.resolve(result.cause.message)
          }
        }
      }
      .addOnFailureListener { throwable ->
        promise.reject(throwable)
      }
  }

  @ReactMethod
  fun getToken(promise: Promise) {
    RuStorePushClient.getToken()
      .addOnSuccessListener { token ->
        promise.resolve(token)
      }
      .addOnFailureListener { throwable ->
        promise.reject(throwable)
      }
  }

  @ReactMethod
  fun deleteToken(promise: Promise) {
    RuStorePushClient.deleteToken()
      .addOnSuccessListener {
        promise.resolve(true)
      }
      .addOnFailureListener { throwable ->
        promise.reject(throwable)
      }
  }

  @ReactMethod
  fun subscribeToTopic(topic: String, promise: Promise) {
    RuStorePushClient.subscribeToTopic(topic)
      .addOnSuccessListener {
        promise.resolve(true)
      }
      .addOnFailureListener { throwable ->
        promise.reject(throwable)
      }
  }

  @ReactMethod
  fun unsubscribeFromTopic(topic: String, promise: Promise) {
    RuStorePushClient.unsubscribeFromTopic(topic)
      .addOnSuccessListener {
        promise.resolve(true)
      }
      .addOnFailureListener { throwable ->
        promise.reject(throwable)
      }
  }

  @ReactMethod
  fun createPushEmitter() {
    if (RustorePushService.emitter != null) return

    RustorePushService.emitter = object : RuStorePushEmitter {

      override fun onNewToken(token: String) {
        val params = WritableNativeMap().apply {
          putString("token", token)
        }
        sendEvent(reactApplicationContext, PushEvents.ON_NEW_TOKEN.name, params)
      }

      override fun onMessageReceived(message: RemoteMessage) {
        val notification = WritableNativeMap().apply {
          putString("title", message.notification?.title)
          putString("body", message.notification?.body)
          putString("channelId", message.notification?.channelId)
          putString("imageUrl", message.notification?.imageUrl.toString())
          putString("color", message.notification?.color)
          putString("icon", message.notification?.icon)
          putString("clickAction", message.notification?.clickAction)
          putString("clickActionType", message.notification?.clickActionType?.name)
        }

        val remoteMessage = WritableNativeMap().apply {
          putString("messageId", message.messageId)
          putInt("priority", message.priority)
          putInt("ttl", message.ttl)
          putString("from", message.from)
          putString("collapseKey", message.collapseKey)
          putMap("data", mapToWritableMap(message.data))
          putArray("rawData",byteArrayToWritableArray(message.rawData))
          putMap("notification", notification)
        }
        sendEvent(reactApplicationContext, PushEvents.ON_MESSAGE_RECEIVED.name, remoteMessage)
      }

      override fun onDeletedMessages() {
        val params = WritableNativeMap().apply {
          putString("callback", "onDeletedMessages")
        }
        sendEvent(reactApplicationContext, PushEvents.ON_DELETED_MESSAGES.name, params)
      }

      override fun onError(errors: String) {
        val params = WritableNativeMap().apply {
          putString("errors", errors)
        }
        sendEvent(reactApplicationContext, PushEvents.ON_ERROR.name, params)
      }
    }
  }

  @ReactMethod
  fun deletePushEmitter() {
    RustorePushService.emitter = null
  }

  @ReactMethod
  fun offNativeErrorHandling() {
    allowNativeErrorHandling = false
  }

  @ReactMethod
  fun sendTestNotification(params: ReadableMap, promise: Promise) {
    val title = params.getString("title").orEmpty()
    val body = params.getString("body").orEmpty()
    val imgUrl = params.getString("imageUrl").orEmpty()
    val data = deconstructReadableMap(params.getMap("data"))

    RuStorePushClient.sendTestNotification(TestNotificationPayload(title, body, imgUrl, data))
      .addOnSuccessListener {
        promise.resolve(true)
      }
      .addOnFailureListener { throwable ->
        promise.reject(throwable)
      }
  }

  @ReactMethod
  fun addListener(eventName: String) {
    // Keep: Required for RN built in Event Emitter Calls.
  }

  @ReactMethod
  fun removeListeners(count: Int) {
    // Keep: Required for RN built in Event Emitter Calls.
  }


  private fun handleNativeError(throwable: Throwable) {
    if (allowNativeErrorHandling && throwable is RuStoreException)
      reactApplicationContext.currentActivity?.let { throwable.resolveForPush(it) }
  }

  private fun sendEvent(reactContext: ReactContext, eventName: String, params: WritableMap?) {
    reactContext
      .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter::class.java)
      .emit(eventName, params)
  }
}
