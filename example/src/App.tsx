import React, { useEffect, useState, useRef, } from 'react';

import RustorePushClient, {
  eventEmitter,
  type RemoteMessage,
  PushEvents,
  TestNotificationPayload
  } from 'react-native-rustore-push';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  type EmitterSubscription,
  TextInput,
  Button,
  Modal,
  Dimensions,
  PermissionsAndroid,
  Platform
} from 'react-native';

const { width } = Dimensions.get("window");

export default function App() {
  const [textPushToken, setText] = useState("");
  const listener = useRef<EmitterSubscription>();

  // dialog subscribe topic
  const [isSubscribeModalVisible, setModalSubscribeVisible] = useState(false);
  const [sudcribeValue, setModalSubscribeValue] = useState("");
  const toggleSubscribeVisibility = () => {
    setModalSubscribeVisible(!isSubscribeModalVisible);
    };

  // dialog unsubscribe topic
  const [isUnsubscribeModalVisible, setModalUnsubscribeVisible] = useState(false);
  const [unsudcribeValue, setModalUnsubscribeValue] = useState("");
  const toggleUnsubscribeVisibility = () => {
    setModalUnsubscribeVisible(!isUnsubscribeModalVisible);
    };

    useEffect(() => {
      requestNotificationPermission();
    }, []);

  const createPushEmitter = () => {
    RustorePushClient.createPushEmitter();

    listener.current = eventEmitter.addListener(PushEvents.ON_MESSAGE_RECEIVED, (message: RemoteMessage) => {
      console.log("messageId", message.messageId)
      console.log("title", message.notification.title)
    });

    return () => {
      listener.current?.remove();
    };
 };

  const checkPushAvailability = async () => {
    try {
      const isAvailable = await RustorePushClient.checkPushAvailability();
      console.log("checkPushAvailability:", isAvailable)
    } catch (err: any) {
      console.log(JSON.stringify(err));
    }
  };

  const getPushToken = async () => {
    try {
      const pushToken = await RustorePushClient.getToken();
      setText(pushToken);
      console.log("pushToken:", pushToken);
    } catch (err: any) {
      console.log(JSON.stringify(err));
    }
  };

  const deletePushToken = async () => {
    try {
      const result = await RustorePushClient.deleteToken();

      console.log("whether the token was deleted:", result);
    } catch (err: any) {
      console.log(JSON.stringify(err));
    }
  };

  const requestNotificationPermission = async () => {
    if (Platform.OS === "android" && Platform.Version >= 33) {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.POST_NOTIFICATIONS,
        {
          title: 'Разрешение на показ уведомлений',
          message: 'Приложению необходимо предоставить разрешение на показ уведомлений',
          buttonNeutral: 'Спросить меня позже',
          buttonNegative: 'Отменить',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Вы можете показывать уведомления');
      } else {
        console.log('Разрешение на показ уведомлений отклонено');
      }
    } catch (err) {
      console.warn(err);
    }
  }
};



  // чтобы получить тестовое уведомление необходимо в классе com.rustorepush.RuStorePushClientParams в функции getTestModeEnabled() вернуть true
  const sendTestNotification = async () => {
    try {
      const testNotification = new TestNotificationPayload(
        'Тестовое уведомление',
        'Здесь будет тело уведомления',
        'https://static.rustore.ru/image/rustore_deeplink_600x315.jpg',
        {key :'key', value: 'value'}
      )
      await RustorePushClient.sendTestNotification(testNotification)
      console.log(JSON.stringify(testNotification));
    } catch (err: any) {
      console.log(JSON.stringify(err));
    }
  };

  const subscribeToTopic = async (value: string) => {
    try {
    const result = await RustorePushClient.subscribeToTopic(value);
    console.log("subscribeToTopic", result);
      // Process subscribe success
  } catch (err) {
      // Process subscribe error
      console.log(JSON.stringify(err));
  }
}

  const unsubscribeFromTopic = async (value: string) => {
    try {
      const result = await RustorePushClient.unsubscribeFromTopic(value);
      console.log("unsubscribeFromTopic", result);
      // Process subscribe success
  } catch (err) {
      // Process subscribe error
      console.log(JSON.stringify(err));
  }
}



  return (
    <View style={styles.screen}>

      <TouchableOpacity onPress={checkPushAvailability}>
        <Text>Проверить доступность получения пушей</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={getPushToken}>
        <Text selectable>Получить пуш токен</Text>
      </TouchableOpacity>

      <Text selectable={true}>{textPushToken}</Text>

      <TouchableOpacity onPress={deletePushToken}>
        <Text selectable={true}>Удалить пуш токен</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={sendTestNotification}>
        <Text>Отправить тестовое уведомление</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={toggleSubscribeVisibility}>
        <Text>Подписаться на топик</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={toggleUnsubscribeVisibility}>
        <Text>Отписаться от топика</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={createPushEmitter}>
        <Text>Создать генератор событий</Text>
      </TouchableOpacity>

      <Modal animationType="slide"
             transparent visible={isSubscribeModalVisible}
             presentationStyle="overFullScreen"
             onDismiss={toggleSubscribeVisibility}>
       <View style={styles.viewWrapper}>
       <View style={styles.modalView}>
       <TextInput placeholder="Enter something..."
             value={sudcribeValue} style={styles.textInput}
             onChangeText={(value) => setModalSubscribeValue(value)} />
      <Button title="Отмена" onPress={ () => {
             setModalSubscribeVisible(false)
        }}/>
       <Button title="Подписаться на топик" onPress={ () => {
             subscribeToTopic(sudcribeValue),
             setModalSubscribeVisible(false)
        }}/>
       </View>
       </View>
      </Modal>

      <Modal animationType="slide"
             transparent visible={isUnsubscribeModalVisible}
             presentationStyle="overFullScreen"
             onDismiss={toggleUnsubscribeVisibility}>
       <View style={styles.viewWrapper}>
       <View style={styles.modalView}>
       <TextInput placeholder="Enter something..."
             value={unsudcribeValue} style={styles.textInput}
             onChangeText={(value) => setModalUnsubscribeValue(value)} />
       <Button title="Отмена" onPress={ () => {
             setModalUnsubscribeVisible(false)
        }}/>
       <Button title="Отписаться от топика" onPress={ () => {
             unsubscribeFromTopic(unsudcribeValue),
             setModalUnsubscribeVisible(false)
        }}/>
       </View>
       </View>
     </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "#fff",
  },
  viewWrapper: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "rgba(0, 0, 0, 0.2)",
  },
  modalView: {
      alignItems: "center",
      justifyContent: "center",
      position: "absolute",
      top: "50%",
      left: "50%",
      elevation: 5,
      transform: [{ translateX: -(width * 0.4) },
                  { translateY: -90 }],
      height: 180,
      width: width * 0.8,
      backgroundColor: "#fff",
      borderRadius: 7,
  },
  textInput: {
      width: "80%",
      borderRadius: 5,
      paddingVertical: 8,
      paddingHorizontal: 16,
      borderColor: "rgba(0, 0, 0, 0.2)",
      borderWidth: 1,
      marginBottom: 8,
  },
});
