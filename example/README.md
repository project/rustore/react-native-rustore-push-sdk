## RuStore React Native SDK для подключения пуш-уведомлений


## [Документация разработчика](https://www.rustore.ru/help/sdk/push-notifications/)


### Оглавление
- [Условия работы push-уведомлений](#Условия-работы-push-уведомлений)
- [Подготовка требуемых параметров](#Подготовка-требуемых-параметров)
- [Настройка примера приложения](#Настройка-примера-приложения)
- [Настройка среды](#Настройка-среды)
- [Сценарий использования](#Сценарий-использования)


### Условия работы push-уведомлений:

1. На устройстве пользователя установлена актуальная версия RuStore.

2. Приложение RuStore должно поддерживать функциональность push-уведомлений.

3. Приложению RuStore разрешен доступ к работе в фоновом режиме.

4. Пользователь авторизован в RuStore.

5. Отпечаток подписи приложения должен совпадать с отпечатком, добавленным в [RuStore Консоль](https://console.rustore.ru/sign-in).


### Подготовка требуемых параметров
Для корректной настройки примера приложения вам следует подготовить:

1. `project_id` проекта пуш-уведомлений из [консоли разработчика](https://console.rustore.ru/apps/2063542111/push/projects/). Чтобы получить его, на странице приложения перейдите в раздел «Push-уведомления» — «Проекты» и скопируйте значение в поле «ID проекта».

    ![Подготовка требуемых параметров](android/app/src/main/res/drawable/project_id.png)

2. `applicationId` - из приложения, которое вы публиковали в консоль RuStore, находится в файле `build.gradle` вашего проекта.

   ```
    android {
       defaultConfig {
          applicationId = "ru.rustore.sdk.pushexample"
       }
    }
   ```

3. `release.keystore` - подпись, которой было подписано приложение, опубликованное в консоль RuStore.

4. `signingConfigs` - параметры подписи, которой было подписано приложение, опубликованное в консоль RuStore. [Как работать с ключами подписи APK-файлов](https://www.rustore.ru/help/developers/publishing-and-verifying-apps/app-publication/apk-signature/).


###  Настройка примера приложения

1. Заменить значение `project_id` в `example/android/app/src/main/AndroidManifest.xml`:

   ```
      <meta-data
        android:name="ru.rustore.sdk.pushclient.project_id"
        android:value="your_project_id" />
   ```

2. Заменить `applicationId`, в директории example/android/app в файле build.gradle, на applicationId apk-файла, который вы публиковали в консоль RuStore:

   ```
   android {
       defaultConfig {
          applicationId = "ru.rustore.sdk.pushexample" // Зачастую в buildTypes приписывается .debug
       }
   }
   ```

3. В директории example/android/app заменить сертификат `release.keystore` - сертификатом своего приложения, так же в `build.gradle` выполнить настройку параметров `storeFile`, `storePassword`, `keyAlias`, `keyPassword`.  Подпись `release.keystore` должна совпадать с подписью, которой было подписано приложение, опубликованное в консоль RuStore. Убедитесь, что используемый `buildType` (пр. debug) использует такую же подпись, что и опубликованное приложение (пр. release).

   ```
   debug {
      storeFile file('release.keystore')
      storePassword 'password'
      keyAlias 'androidreleasekey'
      keyPassword 'password'
   }
   ```


### Настройка среды

1. Необходимо загрузить зависимости, для этого выполните команды `cd example` и следом `npm install`.
2. После того как необходимые зависимости загрузятся, следует выполнить команду `npm start` - для запуска `Metro`.
3. Далее необходимо выбрать `android` для запуска примера в эмуляторе.
4. Если все настроено правильно, вы вскоре увидите, что приложение работает на выбранном устройстве, при условии, что вы правильно настроили свой эмулятор/симулятор.


### Сценарий использования

![Сценарий использования](android/app/src/main/res/drawable/use_case.png)


#### Проверка возможности получать пуш уведомления
Тап по кнопке `Проверить доступность получения пушей` выполняет [проверку возможности получения push-уведомления](https://www.rustore.ru/help/sdk/push-notifications/react/2-1-1#get-push-check). Результат проверки выводится в лог.


#### Нативная обработка ошибок
Отключает [обработку ошибок со стороны](https://www.rustore.ru/help/sdk/push-notifications/react/2-1-1/#errors) сдк, по умолчанию она включена.

Вот так выглядит обработка ошибки при проверке возможности получать пуш уведомления, когда на устройстве не установлен RuStore или хостовое приложение:

![Нативная обработка ошибок](android/app/src/main/res/drawable/handle_native_error.png)


#### Создание генератора событий
На стороне сдк создастся [генератор событий](https://www.rustore.ru/help/sdk/push-notifications/react/2-1-1/#создание-эмиттера). Вы можете подписаться на эти события.
В примере мы [подписались](https://www.rustore.ru/help/sdk/push-notifications/react/2-1-1/#получение-информации-о-push-уведомлениях) на событие `PushEvents.ON_MESSAGE_RECEIVED` - которое отрабатывает при получении пуша, если не создать эмиттер, то события приходить не будут.


#### Получение пуш токена
Тап по кнопке `Получить пуш токен` выполняет процедуру [получения текущего push-токена пользователя](https://www.rustore.ru/help/sdk/push-notifications/react/2-1-1/#get-push-token). Push-токен пользователя, полученный в приложении, используется в структуре `message` для отправки push-уведомлений. Если у пользователя нет push-токена, метод создаст и вернёт новый push-токен.


#### Удаление пуш токена
Запуск сценария [удаляет пуш токен устройства](https://www.rustore.ru/help/sdk/push-notifications/react/2-1-1/#delete-push-token). При следующем вызове метода получения токена - будет создан новый токен.


#### Отправка тестового уведомления
Запуск сценария [отправляет тестовое уведомление](https://www.rustore.ru/help/sdk/push-notifications/react/2-1-1/#test-sdk) на устройство, с которого инициирована отправка.

Чтобы отправить тестовое уведомление, необходимо в `RuStorePushClientParams` в функции `getTestModeEnabled` вернуть `true`, по умолчанию возвращает `false`.

   ```
   class RuStorePushClientParams(context: Context) : AbstractRuStorePushClientParams(context) {

     override fun getLogger(): Logger = DefaultLogger("ReactRuStorePushClient")

     override fun getTestModeEnabled(): Boolean = true
   }
   ```

В этом режиме формируется тестовый push-токен и работает доставка только тестовых push-уведомлений.

![Отправка тестового уведомления](android/app/src/main/res/drawable/send_test_notification.png)


#### Подписка на топик
Запуск сценария [подписки на push-уведомления по топику](https://www.rustore.ru/help/sdk/push-notifications/react/2-1-1/#push-topic-subscribe).


#### Отписка от топика
Запуск сценария [отписки от push-уведомлений по топику](https://www.rustore.ru/help/sdk/push-notifications/react/2-1-1/#push-topic-unsubscribe).
