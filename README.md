## RuStore React Native SDK для подключения пуш-уведомлений

### [🔗 Документация разработчика](https://www.rustore.ru/help/sdk/push-notifications/)

SDK RuStorePush предоставляет функциональность для включения в приложение push-уведомлений через сервисы RuStore.


### Подключение в проект

```sh
// HTTPS
npm install git+https://git@gitflic.ru/project/rustore/react-native-rustore-push-sdk.git
// SSH
npm install git+ssh://git@gitflic.ru/project/rustore/react-native-rustore-push-sdk.git
```


### Сборка примера приложения

Вы можете ознакомиться с демонстрационным приложением содержащим представление работы всех методов sdk:
- [README](example/README.md)
- [example](https://gitflic.ru/project/rustore/react-native-rustore-push-sdk/file?file=example&branch=master)


### Условия распространения
Данное программное обеспечение, включая исходные коды, бинарные библиотеки и другие файлы распространяется под лицензией MIT. Информация о лицензировании доступна в документе [MIT-LICENSE](MIT-LICENSE.txt).


### Техническая поддержка
Дополнительная помощь и инструкции доступны на странице [help.rustore.ru](https://help.rustore.ru/).
